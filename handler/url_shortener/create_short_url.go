package handler

import (
	"galahad/entity"
	"github.com/apex/log"
	"github.com/labstack/echo/v4"
	"net/http"
)

func (handler *URLShortenerHandlerImpl) CreateShortURL(ctx echo.Context) error {
	urlShortenerRequest := entity.URLShortenerRequest{}

	err := ctx.Bind(&urlShortenerRequest)
	if err != nil || urlShortenerRequest.URL == "" || urlShortenerRequest.UserID == "" {
		if err != nil {
			log.Errorf("Error create URL reading %v", err)
		}

		return echo.NewHTTPError(http.StatusBadRequest, "Bad URL Shortener Request, "+
			"user_id and url can't be empty")
	}
	shortURL := handler.urlShortenerUseCase.CreateShortURL(urlShortenerRequest.URL, urlShortenerRequest.UserID)
	log.Infof("Short URL %v from %v is successfully created", shortURL, urlShortenerRequest.URL)
	return ctx.JSON(http.StatusCreated, entity.URLShortenerCreateResponse{
		ShortURL: shortURL},
	)
}
