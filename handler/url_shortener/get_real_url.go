package handler

import (
	"fmt"
	"galahad/entity"
	"github.com/labstack/echo/v4"
	"net/http"
)

func (handler *URLShortenerHandlerImpl) GetRealURL(ctx echo.Context) error {
	shortURL := ctx.Param("shortURL")
	realURL := handler.urlShortenerUseCase.GetRealURL(shortURL)

	if realURL == "" {
		return echo.NewHTTPError(http.StatusNotFound,
			fmt.Sprintf("Can't found URL: %v", shortURL))
	}

	return ctx.JSON(http.StatusOK, entity.URLShortenerGetResponse{
		ShortURL:    shortURL,
		OriginalURL: realURL,
	})
}
