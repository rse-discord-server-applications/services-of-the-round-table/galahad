package handler

import (
	"galahad/handler"
	usecase "galahad/usecase/url_shortener"
	"github.com/labstack/echo/v4"
)

type URLShortenerHandler interface {
	CreateShortURL(ctx echo.Context) error
	GetRealURL(ctx echo.Context) error
}

type URLShortenerHandlerImpl struct {
	urlShortenerUseCase usecase.URLShortenerUseCase
	manager             *echo.Echo
	handler.Handler
}

func NewURLShortenerHandlerImpl(echo *echo.Echo, urlShortenerUseCase usecase.URLShortenerUseCase) *URLShortenerHandlerImpl {
	urlShortener := &URLShortenerHandlerImpl{urlShortenerUseCase: urlShortenerUseCase, manager: echo}
	urlShortener.DefineEndpoint()
	return urlShortener
}

func (handler *URLShortenerHandlerImpl) DefineEndpoint() {
	urlShortenerGroup := handler.manager.Group("/api/url-shortener")
	urlShortenerGroup.POST("/create", handler.CreateShortURL)
	urlShortenerGroup.GET("/get/:shortURL", handler.GetRealURL)

}
