package handler

import (
	"galahad/handler"
	"github.com/labstack/echo/v4"
	"net/http"
)

//go:generate mockgen -destination=health_check_mocks_test.go -package=handler -source=health_check.go

type HealthCheckHandler interface {
	HealthCheck(ctx echo.Context) error
}

type HealthCheckHandlerImpl struct {
	handler.Handler
	manager *echo.Echo
}

func NewHealthCheckHandlerImpl(echo *echo.Echo) *HealthCheckHandlerImpl {
	healthCheckHandler := HealthCheckHandlerImpl{manager: echo}

	healthCheckHandler.DefineEndpoint()

	return &healthCheckHandler

}

func (h *HealthCheckHandlerImpl) DefineEndpoint() {
	healthGroup := h.manager.Group("/api/health-check")
	healthGroup.GET("/status", h.HealthCheck)
}

func (h *HealthCheckHandlerImpl) HealthCheck(ctx echo.Context) error {
	return ctx.JSON(http.StatusOK, "healthy")
}
