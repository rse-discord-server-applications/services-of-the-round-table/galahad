
module.exports = {
    apps: [
        {
            name: 'galahad',
            script: './galahad',
            watch: false,
            force: true,
            log_date_format: "DD-MM-YYYY HH:mm Z",

        },
    ],
};