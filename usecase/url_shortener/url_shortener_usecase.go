package usecase

import repository "galahad/repository/url_shortener"

type URLShortenerUseCase interface {
	CreateShortURL(realUrl string, userId string) string
	GetRealURL(shortUrl string) string
}

type URLShortenerUseCaseImpl struct {
	urlShortenerRepo repository.URLShortenerRepo
}

func NewURLShortenerUseCaseImpl(urlShortenerRepo repository.URLShortenerRepo) *URLShortenerUseCaseImpl {
	return &URLShortenerUseCaseImpl{urlShortenerRepo: urlShortenerRepo}
}
