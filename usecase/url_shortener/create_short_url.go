package usecase

import "galahad/utils"

func (u *URLShortenerUseCaseImpl) CreateShortURL(realUrl string, userId string) string {
	shortUrl := utils.GenerateShortLink(realUrl, userId)
	u.urlShortenerRepo.SaveURL(shortUrl, realUrl)
	return shortUrl
}
