package usecase

import "github.com/apex/log"

func (u *URLShortenerUseCaseImpl) GetRealURL(shortURL string) string {
	realUrl := u.urlShortenerRepo.GetRealURL(shortURL)
	if realUrl == "" {
		log.Errorf("The URL you are looking for is not exists")
		// @TODO handle null
	}
	return realUrl
}
