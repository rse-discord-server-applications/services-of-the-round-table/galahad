package repository

import "github.com/jellydator/ttlcache/v3"

const (
	DefaultTTL = ttlcache.NoTTL
)
