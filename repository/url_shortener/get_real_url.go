package repository

func (u *URLShortenerRepoImpl) GetRealURL(shortUrl string) string {
	itemFromCache := u.cache.Get(shortUrl)
	if itemFromCache == nil {
		return ""
	}
	return itemFromCache.Value()
}
