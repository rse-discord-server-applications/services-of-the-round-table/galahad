package repository

import "github.com/jellydator/ttlcache/v3"

type URLShortenerRepo interface {
	SaveURL(shortUrl string, realURL string)
	GetRealURL(shortUrl string) string
}

type URLShortenerRepoImpl struct {
	cache *ttlcache.Cache[string, string]
}

func NewURLShortenerRepoImpl() *URLShortenerRepoImpl {
	return &URLShortenerRepoImpl{
		cache: ttlcache.New[string, string](),
	}
}
