package repository

import "galahad/repository"

func (u *URLShortenerRepoImpl) SaveURL(shortUrl string, realURL string) {
	u.cache.Set(shortUrl, realURL, repository.DefaultTTL)
}
