package entity

type URLShortenerRequest struct {
	URL    string `json:"url"`
	UserID string `json:"user_id"`
}

type URLShortenerCreateResponse struct {
	ShortURL string `json:"short_url"`
}

type URLShortenerGetResponse struct {
	ShortURL    string `json:"short_url"`
	OriginalURL string `json:"original_url"`
}
