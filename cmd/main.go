package main

import (
	"fmt"
	"galahad/config"
	healthCheckHandler "galahad/handler/health_check"
	urlShortenerHandler "galahad/handler/url_shortener"
	repository "galahad/repository/url_shortener"
	usecase "galahad/usecase/url_shortener"
	"github.com/apex/log"
	"github.com/apex/log/handlers/multi"
	"github.com/apex/log/handlers/text"
	"github.com/labstack/echo/v4"
	"os"
)

var appConfig = config.GetConfig()

func main() {

	InitLog()
	e := echo.New()

	// Repository
	urlShortenerRepo := repository.NewURLShortenerRepoImpl()

	// UseCase
	urlShortenerUseCase := usecase.NewURLShortenerUseCaseImpl(urlShortenerRepo)

	//Handler
	urlShortenerHandler.NewURLShortenerHandlerImpl(e, urlShortenerUseCase)
	healthCheckHandler.NewHealthCheckHandlerImpl(e)

	port := fmt.Sprintf(":%v", appConfig.Port)

	log.Infof("Application started at port %v", appConfig.Port)

	err := e.Start(port)
	if err != nil {
		log.Errorf("Error starting server reading %v", err)
	}

}

func InitLog() {

	log.SetLevel(log.InfoLevel)
	if appConfig.Env == "dev" {
		log.SetLevel(log.DebugLevel)
	} else if appConfig.Env == "prod" {
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(log.ErrorLevel)
	}

	log.SetHandler(multi.New(
		text.New(os.Stdout)))
}
