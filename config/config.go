package config

import "os"

type Config struct {
	Env     string
	Port    string
	LogPath string
}

func GetEnv(key string, defaultValue string) string {
	value, exists := os.LookupEnv(key)
	if !exists {
		value = defaultValue
	}
	return value
}

func GetConfig() Config {
	env := GetEnv("ENV", "prod")
	port := GetEnv("PORT", "8080")
	path := GetEnv("LOG_PATH", "logs")

	return Config{
		Env:     env,
		Port:    port,
		LogPath: path,
	}
}
